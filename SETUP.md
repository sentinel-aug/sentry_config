# Setting up machines for use with ROSLAUNCH

roslaunch is ridiculously stupid when it comes to launching things remotely.  You need to do the following:
* Specify a ROS_IP for your OWN ip address, so it doesn't use a hostname.  Because, you know, field robots always have DNS.
* Specify a ROS_MASTER_URI on most machines
* Setup ssh logins (see below)

We handle the command line variables with our own setup script.

## Setting up ssh

So roslaunch uses the most absurdly silly library it could find, and you have to pre-share keys with an old algorithm.  See: https://answers.ros.org/question/244060

The magic incantations are:

```
# On the target machine:
ssh-keygen

# On originating machine
ssh -oHostKeyAlgorithms='ssh-rsa' user@target.ip.goes.here
ssh-copy-id user@target.ip.goes.here
```


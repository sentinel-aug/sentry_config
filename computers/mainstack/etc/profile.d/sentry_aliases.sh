#2009-08-03 JCK - commands to turn the timer trigger on and off
#alias timon="/home/sentry/code/rov/sentry_cmds/cmd timon"
#alias timoff="/home/sentry/code/rov/sentry_cmds/cmd timoff"
#2016-07-26 ZB - Switched to python-installed tim.py script
#alias timon="tim.py on tim"
#alias timoff="tim.py off tim"
#2017-04-12 ZB - Switched to python-installed setup_trg.py script for new 8 channel trigger
alias timon="setup_trg.py on /dev/ttyS31"
alias timoff="setup_trg.py off /dev/ttyS31"

alias mc="cd /home/sentry/code/mc/src"
alias bat-cmd="python /home/sentry/code/sentrybattery/bat-cmd/bat-cmd.py"

#2014-05-02 JRH - added process search script for checklist
# alias find_processes="python /home/sentry/sentry-utils/pre-dive-automation/find_process.py"	
#2015-04-08 ZB - Added find_process2 for testing
#2015-06-21 ZB - Made default
alias find_processes="python /home/sentry/code/sentry-utils/pre-dive-automation/find_process2.py"	
#2014-05-02 JRH - added log checker script for checklist
alias check_logs="python /home/sentry/code/sentry-utils/pre-dive-automation/check_data_logging.py"	
#08-01-2016 ZB  Alias for upstart-controlled daemons
# reson driver
# alias goResonDriver="initctl start sentry/reson-driver"
# alias stopResonDriver="initctl stop sentry/reson-driver"

# umodem driver
# alias goAcomms="initctl start sentry/acomm"
# alias stopAcomms="initctl stop sentry/acomm"

#2014-05-02 JRH - added log phins cal for checklist
#2014-05-02 JRH - added log phins cal for checklist
#alias phins_cal="python /home/sentry/code/rov/sentry_cmds/phins/phinsmanpos.py"	
# 2021-04-10 SS - moved to sentry-util to eliminate rov dependency
alias phins_cal="python /home/sentry/code/sentry-utils/phins/phinsmanpos.py"	

#2014-12-01 - added query_xr
#alias query_xr='python /home/sentry/code/sentry-utils/pre-dive-automation/query_xr.py'

alias timesync='python /home/sentry/code/sentry-utils/irig/sync_irig.py'
#alias safesentry='bash /home/sentry/sentry_ws/devel/safesentry_onsentry.bash'

# 2018-05-09 JLV- added some ros shortcuts, commented some obsolete cmds
# launch and secure cmds
alias secure='bash /home/sentry/sentry_ws/devel/secure_onsentry.bash'
alias launch_hotel='bash /home/sentry/sentry_ws/devel/launch_hotel_onsentry.bash'
alias launch_hotel_decktest='bash /home/sentry/sentry_ws/devel/launch_hotel_decktest_onsentry.bash'
alias launch_vehicle='bash /home/sentry/sentry_ws/devel/launch_vehicle_onsentry.bash'
alias launch_vehicle_decktest='bash /home/sentry/sentry_ws/devel/launch_vehicle_decktest_onsentry.bash'
alias launch_vehicle_welltest='bash /home/sentry/sentry_ws/devel/launch_vehicle_welltest_onsentry.bash'
# tmux cmds
alias tlaunch='sh /home/sentry/sentry_ws/devel/tlaunch-sentry.sh'
alias tcon_hotel='sh /home/sentry/sentry_ws/devel/tconnect.sh sentry hotel'
alias tcon_vehicle='sh /home/sentry/sentry_ws/devel/tconnect.sh sentry vehicle'
alias tcon_mc='sh /home/sentry/sentry_ws/devel/tconnect.sh sentry mc'
alias tcon_kongsberg='sh /home/sentry/sentry_ws/devel/tconnect.sh sentry kongsberg'
# snuff and zero phins cmds
alias snuff='bash /home/sentry/sentry_ws/devel/snuff_onsentry.bash'
alias zeroparo='bash /home/sentry/sentry_ws/devel/zeroparo_onsentry.bash'
alias indexservos='bash /home/sentry/sentry_ws/devel/indexservos_onsentry.bash'

alias sentry='ssh sentry@192.168.100.100 -X'
alias uptopnav='sudo python /home/sentry/code/topside-navest/navest/bin/update_topside_nav.py'
alias dpb='ssh sentry@192.168.100.161 -X'
alias dpa='ssh sentry@192.168.100.160 -X'
alias dp1='ssh sentry@192.168.100.7 -X'
alias dp2='ssh sentry@213.123.1.192 -X'
alias datapod='ssh sentry@192.168.100.101 -X'
alias camera='ping 192.168.100.102'
alias get_photos='rsync sentry@192.168.100.101:~/disk/*.tif ~/Desktop/'
alias code='cd /home/sentry/code'
alias kctrl='docker run --rm -it --network host kctrl'

alias sgit='git -c user.name="Stefano Suman" -c user.email="ssuman@whoi.edu"'
alias igit='git -c user.name="Ian Vaughn" -c user.email="ivaughn@whoi.edu"'
alias jgit='git -c user.name="Jennifer Vaccaro" -c user.email="jvaccaro@whoi.edu"'
alias lgit='git -c user.name="Laura Lindzey" -c user.email="llindzey@whoi.edu"'
alias zgit='git -c user.name="Zac Berkowitz" -c user.email="zberkowitz@whoi.edu"'
alias vgit='git -c user.name="Isaac Vandor" -c user.email="ivandor@whoi.edu"'
alias jogit='git -c user.name="Joe Garcia" -c user.email="jogarcia@whoi.edu"'

# LEL added 2019-Nov-4; I'm sick of typing all of these every time I need a ROS terminal.
# For setting up DPA as the master
alias source_dpa='source /home/sentry/sentry_ws/devel/setup.bash'
# For setting up a terminal on sentry
alias source_sentry='source /home/sentry/sentry_ws/devel/sentry_setup.bash'
# For setting up ws2 with sentry as master
alias source_workstation='source /home/sentry/sentry_ws/devel/workstation_setup.bash'
# For setting up ws2 with DPA as master
alias source_topside='source /home/sentry/sentry_ws/devel/workstation_setup.bash; export ROS_MASTER_URI=http://192.168.100.160:11311'

alias sentrysitter='/home/sentry/sentry_ws/devel/sentrysitter_workstation.bash'

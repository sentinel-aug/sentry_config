#!/usr/bin/env python

import ds_param

class CmdAlloc(object):
    """Implements a command to control the allocation type"""

    def __init__(self):
        self._contype = None

    def parse_args(self, args):
        """Parse all arguments.  Either throw an execption, or be ready-to-run"""
        if not args:
            raise ValueError('No allocation type specified!')

        self._contype = int(args[0])

    def execute(self):
        """Actually execute the commands.  Args is a list of string arguments from argparse"""

        print 'Setting sentry''s allocation type to \"%d\"' % self._contype

        conn = ds_param.ParamConnection()

        # TODO: Actually USE this!
        # You know, once EnumParam is actually implemented
        p_contype  = conn.connect('/sentry/controllers/active_allocation' , ds_param.EnumParam, False)
        p_contype.set(self._contype)

    @staticmethod
    def help():
        print """
        
        Set Sentry's allocation type.   The command is:
        
        sentry_cmd con [allocation number] 
        
        Options are determined at runtime, but initially might be:
        
        0 -- idle allocation
        1 -- flight allocation
        2 -- rov allocation
        3 -- vertical allocation
        
        """

    @staticmethod
    def args_help():
        return '[allocation number]'

# Expose this class to the wider world
CmdName = 'ALLOC'
CmdClass = CmdAlloc


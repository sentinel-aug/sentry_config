#!/usr/bin/env python

import ds_param

class CmdGoalParam(object):
    """Implements a command to control a goal param node"""

    def __init__(self):

        self._param = None
        self._value = None

    def parse_args(self, args):
        """Parse all arguments.  Either throw an execption, or be ready-to-run"""
        if not args:
            raise ValueError('No parameters supplied')


        if len(args) < 2:
            raise ValueError('Expected $param_name $value pair')

        self._param = args[0]

        if "valid" in self._param:
            self._value = int(args[1])
        else:
            self._value = float(args[1])


    def execute(self):
        """Actually execute the commands.  Args is a list of string arguments from argparse"""

        print 'Setting parameter \"%s\" to %s' % (self._param, self._value)

        conn = ds_param.ParamConnection()

        if "valid" in self._param:
            p  = conn.connect(self._param , ds_param.BoolParam, False)
        else:
            p  = conn.connect(self._param , ds_param.DoubleParam, False)

        p.set(self._value)

    @staticmethod
    def help():
        print """
        
        Set goal parameter value.   The command is:
        
        sentry_cmd goal_param [param_name] [param_value]
        
        """

    @staticmethod
    def args_help():
        return '[param_name] [param_value]'

# Expose this class to the wider world
CmdName = 'GOAL_PARAM'
CmdClass = CmdGoalParam


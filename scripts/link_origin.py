#!/usr/bin/env python

import argparse
import glob
import os.path

# BASH has no good sorting implementation, so we're going to use
# python to get the most recent

parser = argparse.ArgumentParser(description='Link the origin for the highest-numbered dive to the place where ROS will look for it')
parser.add_argument('source')
parser.add_argument('destination')
args = parser.parse_args()

files = sorted(glob.glob(os.path.join(args.source, 'sentry*_ros_org.yaml')), reverse=True)

source = os.path.abspath(files[0])
dest   = os.path.abspath(args.destination)
print '+-----------------------------------------------------------+'
print '| Linking origin file:                                      |'
print '|   %-50s      |' % source
print '| to:                                                       |'
print '|   %-50s      |' % dest
print '+-----------------------------------------------------------+'
cmd = 'ln -sf %s %s' % (source, dest)
#print "\n\"%s\"" % cmd
ret = os.system(cmd)
if ret != 0:
  raise Exception("Unable to link origin!")


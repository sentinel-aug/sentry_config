#!/bin/bash

# Get the directory this script is located in
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Look through ALL my own IPs and pick the one matching our subnet
MY_IP="$(hostname -I | grep -oP "(192\.168\.100\.[0-9]+)")"

# YAY! Debug info!
echo "Setting my IP address to $MY_IP"

# Load the standard ROS setup
source $DIR/setup.bash

# Set my own IP based on my own IP
export ROS_IP=$MY_IP

# Connect to the master running on Sentry
export ROS_MASTER_URI=http://192.168.100.100:11311/
